#!/bin/sh

#this script copies the incoming radarimages to their webpage folder
sourceimages=/media/sf_Radar_Gifs
tempdirectory=/var/www/radarviewerprototype/radarimagestemp
#destinationimages=/var/www/radarviewerprototype/WebContent/areas/LOXX/radarimages/3hours
destinationimages=/var/www/radarviewerprototype/WebContent/areas
historicalarchive=/var/www/radarviewerprototype/WebContent/historicalarchive/

filenamelength=21

AREAS=(LOXX GUAXX CAXX)
RESOLUTIONS=( LOXX LOX1 LOX3 GUAXX GUAX1 GUAX3 CAXX CAX1 CAX3 ) #60km 30km 15km
TIMESPANS_MIN=(180)

#Loja =60km 3h:  LOXX areas/LOXX/radarimages/3h/
#wildcard_loxx=LOXX*.gif
#Celica = GUAXX
#Cajas = CAXX

#0. vorhandene Verzeichnisse (in for schleife, nur die xxxkm!) löschen?

#1. move all images from source to temp. then only work on those. (keep incoming images untouched during this run)
mkdir $tempdirectory
#later active! mv $sourceimages/*.gif $tempdirectory mv is changing the modified date!
#here only copy files not older than 1 day. this will make this script much faster.
#cp -p $sourceimages/*.gif $tempdirectory
find $sourceimages/*.gif -maxdepth 1 -type f -mtime -1 -exec cp -p '{}' $tempdirectory \;
#muessen eigentlich garnicht gelöscht werden? evtl. wird der ordner zu gross?! or parse datetime from filename rm $sourceimages/*.gif 


#0 run prerename script for GUAXX files to copy files to tempdirectory
/bin/bash /var/www/radarviewerprototype/scripts/preorganizeguaxx.sh $tempdirectory
/bin/bash /var/www/radarviewerprototype/scripts/preorganizecaxx.sh $tempdirectory

#1a. remove files from tempdirectory, whose filenames do not have the correct length
for file in $tempdirectory/*
do
	filename=$(basename "$file")
#	printf "%s\t%s\t%s\n" "$filename" ${#filename}
	if [ ${#filename} -gt $filenamelength ]; then
		echo "Filename too long, delete file: " $file
		rm "$file"
	fi
done

#2. distribute images to website

#echo copy $sourceimages/$wildcard_loxx to $destinationimages
#for area in "${!AREAS[@]}"; do
for (( i = 0 ; i < ${#AREAS[@]} ; i++ )) 
do
	printf "%s\t%s\n" "$i" "${AREAS[$i]}" 
#	for res in "${RESOLUTIONS[@]}" # iterate over index of 1. for +1 * 3 in RESOLUTIONS
	for (( u=i*3 ; u < i*3+3 ; u++))
	do
		#hier 3 forschleifen: for file in `find $destinationimages+wildcard -type f -mmin +90`; do
		printf "%s\t%s\n" "${RESOLUTIONS[$u]}" "${u}"
		for (( x=0 ; x < ${#TIMESPANS_MIN[@]} ; x++))
		do
			printf "%s\t%s\t%s\n" "${TIMESPANS_MIN[$x]}" "$tempdirectory/${RESOLUTIONS[$u]}*.gif"
			mkdir -p $destinationimages/${RESOLUTIONS[$u]}/radarimages/${TIMESPANS_MIN[$x]}min
			for file in `find $tempdirectory/${RESOLUTIONS[$u]}*.gif -type f -mmin -${TIMESPANS_MIN[$x]}`
			do
				printf "%s\t%s\n" "$file" "$destinationimages/${RESOLUTIONS[$u]}/radarimages/${TIMESPANS_MIN[$x]}min"
				chmod o+r $file
				cp -p $file $destinationimages/${RESOLUTIONS[$u]}/radarimages/${TIMESPANS_MIN[$x]}min
			done
			#2b. housekeeping: remove files which are older than TIMESPANS_MIN
			for file in `find $destinationimages/${RESOLUTIONS[$u]}/radarimages/${TIMESPANS_MIN[$x]}min/*.gif -type f -mmin +${TIMESPANS_MIN[$x]}`
			do
				printf "%s%s\t\n" "Remove old file:" "$file"
				rm $file
			done
		done
	done
done

#3. move all images from temp to historical archive
#3b. Advanced: create RESOLUTIONS/YYYY/MM/ subfolders (problematic: border between two days or months)
cp -p $tempdirectory/*.gif $historicalarchive
rm -r $tempdirectory
