<h2>Radar LOXX</h2>
(Cerro El Tiro, 2850m asl)
<br/>
<img src="images/radar_loxx.jpg"></img>
<br/>
Manufacture: DHI (Dansk Hydrolosk Institut)<br/>
Local Area Weather Radar (LAWR)<br/>
Link: <a href="http://radar.dhigroup.com/">http://radar.dhigroup.com/</a>
<br/><br/>
<b>Specifications:</b><br/>
<table>
	<tr><td>Radar type:</td><td>X-band radar (9.4Ghz)</td></tr>
	<tr><td>Output power:</td><td>25kW</td></tr>
	<tr><td>Beam width (H):</td><td>0.92&deg;</td></tr>
	<tr><td>Beam width (V):	</td><td>10&deg; up and 10&deg; down</td></tr>
	<tr><td>Rotation speed:	</td><td>25rpm</td></tr>
	<tr><td>Range:</td><td>60km radius (max.)</td></tr>
	<tr><td>Image frequency:</td><td>5 minutes (3 images)</td></tr>
	<tr><td>Image range and pixel size:</td><td></td></tr>
	<tr style="column-span: 2;"><td></td><td>
		<table>
			<tr><th>Range</th><th>Resolution</th><th>Image name</th></tr>
			<tr><td>60km</td><td>500m x 500m</td><td>LOXX</td></tr>
			<tr><td>30km</td><td>250m x 250m</td><td>LOX1</td></tr>
			<tr><td>15km</td><td>100m x 100m</td><td>LOX3</td></tr>		
		</table>
		</td>
	</tr>
</table>

<br/>
<br/>

<h2>Radar GUAXX</h2>
(Cerro Guachaurco, 3100m asl)
<br/>
<h2>Radar CAXX</h2>
(Cerro Paraguillas, 4440m asl)
<br/>
<img src="images/radar_guaxx_caxx.jpg"></img>
<br/>
Manufacture: Selex - Gematronik<br/>
RainScanner (RS 120)<br/>
Link: <a href="http://www.gematronik.com/products/radar-systems/rainscanner/">http://www.gematronik.com/products/radar-systems/rainscanner/</a>
<br/><br/>
<b>Specifications:</b><br/>
<table>
	<tr><td>Radar type:</td><td>X-band radar (9.4Ghz)</td></tr>
	<tr><td>Output power:</td><td>25kW</td></tr>
	<tr><td>Beam width (H):</td><td>0.5&deg; ... 2&deg;	</td></tr>
	<tr><td>Beam width (V):	</td><td>1&deg; up and 1&deg; down</td></tr>
	<tr><td>Rotation speed:	</td><td>12rpm</td></tr>
	<tr><td>Range:		</td><td>100km radius (max.)</td></tr>
	<tr><td>Image characteristics:</td><td></td></tr>
	<tr><td>Typical Resolution: </td><td>Radial 100 m to 1 km</td></tr>
	<tr><td>Tangential</td><td>0.5 to 2 &deg;</td></tr>
	<tr><td>Temporal </td><td>15 sec to 10 min</td></tr>
</table>