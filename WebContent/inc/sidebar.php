					<?php
						function folderCheck($directory){
							$tmp = 'style="display:none"';
							$directories = glob($directory . '/*' , GLOB_ONLYDIR);
                        	$dir = $directories[0];
							if (is_dir($dir)) {
							    if ($dh = opendir($dir)) {
							        while (($file = readdir($dh)) !== false) {
							        	if($file != "." && $file != ".." && $file != ".svn"){
							            	$tmp = '';
							        		break;
							        	}
							        }
							        closedir($dh);
							    }
							}
							echo $tmp;
						}
						function showNaviEntries($directory, $id){
							$stringArrayBefore = array("180min", "90min");
							$stringArrayAfter = array("Loop latest 3 hours", "Loop latest 90 minutes");
							$directories = glob($directory . '/*' , GLOB_ONLYDIR);
							$linkName = '';
							for ($i = 0; $i < count($directories); $i++) {
                            	$tmp = substr($directories[$i],strrpos($directories[$i],'/')+1);
                            	for ($j = 0; $j < count($stringArrayBefore); $j++) {
                        			if($tmp == $stringArrayBefore[$j]){
                        				$linkName = $stringArrayAfter[$j];
                        				break;
                        			}else{
                        				$linkName = $tmp;
                        			}
                        		}
                        		echo '<li>';
								echo '<a href="index.php?radar_id='.$id.'&amp;radar_folder='.$tmp.'" class="picLink subfolder_'.$tmp.'">';
								echo $linkName;
								echo '</a>';
								echo '</li>';
							}
						}
					?>
					<ul>
                        <li class="SE">
                        	<p>
                            	Southern Ecuador
                            </p>
	                        <div class="listfolder">
	                            <ul>
	                                <li>
	                                    <a href="index.php?radar_id=SE" class="picLink subfolder_default">Main screen</a>
	                                </li>
	                            </ul>
	                        </div>                            
                        </li>
                        <li class="LOXX" <?php folderCheck('areas/LOXX/radarimages'); ?>>
                        	<p>
                            	Loja (LOXX, 60km)
                            </p>
	                        <div class="listfolder">
	                            <ul>
	                            	<?php
	                            		showNaviEntries('areas/LOXX/radarimages',"LOXX");
                                	?>
	                            </ul>
	                        </div>
                        </li>
                        <li class="GUAXX" <?php folderCheck('areas/GUAXX/radarimages'); ?>>
                        	<p>
                            	Celica (GUAXX, 100km)
                            </p>
	                        <div class="listfolder">
	                            <ul>
	                                <?php
	                            		showNaviEntries('areas/GUAXX/radarimages',"GUAXX");
                                	?>
	                            </ul>
	                        </div>
                        </li>
                        <li class="CAXX" <?php folderCheck('areas/CAXX/radarimages'); ?>>
                        	<p>
                            	Cajas (CAXX, 100km)
                            </p>
	                        <div class="listfolder">
	                            <ul>
	                                <?php
	                            		showNaviEntries('areas/CAXX/radarimages',"CAXX");
                                	?>
	                            </ul>
	                        </div>
                        </li>
                        <li>
                        	<p>
                        		Information
                        	</p>
                        	<div class="listfolder">
	                            <ul>
	                                <li>
	                                    <a href="index.php?n=instrument_info">Radar specifications</a>
	                                </li>
	                                <li>
	                                    <a href="index.php?n=principles">Functional principle of a meteorological radar</a>
	                                </li>
	                            </ul>
	                        </div>
                        </li>
                    </ul>
