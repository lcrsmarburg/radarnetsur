<?php
function readFolder($pfad){
        $handle = opendir($pfad);
        $dirxml = "";
    	$tmpArray = array();
        while ($file = readdir ($handle)) {
			if($file != "." && $file != ".." && checkFileExtension($file)){
        			array_push($tmpArray,sprintf("%s", $file));
			}
        }
        sort($tmpArray);
        for($i=0;$i<sizeof($tmpArray);$i++){
        	$dirxml .= ''.$tmpArray[$i].', ';
        }
        echo substr($dirxml, 0, -2);
        closedir($handle);
}
function checkFileExtension($f){
	if(!strrpos($f, "jpg") && !strrpos($f, "png") && !strrpos($f, "gif") && !strrpos($f, "jpeg") && !strrpos($f, "bmp")){
		return false;
	}
	return true;
}
//Parameter muss vor dem include gesetzt werden, sonst wird der GET-Parameter geprüft
if(!$folder){
	$folder = htmlspecialchars($_GET["folder"]);
}
if($folder != "" && !strrpos($folder, "..") && substr($folder,0,1) != "/"){
	readFolder($folder);
}else{
	echo "Fehler! Zu wenig Parameter!";
}
?>
