var objectArray = new Array();
var txt = "";
var speed = 1000;
var activeContainer;
var activeObject;
var index = 0;
var t;
var picLoadCounter = 0;
var startupVariable = 0;
String.prototype.fileExists = function(){
    filename = this.trim();
    var response = jQuery.ajax({
        url: filename,
        type: 'HEAD',
        async: false
    }).status;
    return (response != "200") ? false : true;
};

function setOverlays(id){
	console.info("setOverlays: "+id);
    var overlayArray = new Array("background", "terrain", "rivers", "towns", "roads", "special", "shadow");
    var notFound = 0;
	$("#ajaxError").hide();
	$("#radarElements").show();
    for (var i = 0; i < overlayArray.length; i++) {
        var url = 'areas/' + id + '/overlays/' + overlayArray[i] + '.png';
        if (url.fileExists()) {
            $("#" + overlayArray[i] + "Overlay").html('<img src="' + url + '" alt=""/>').show();
			if (overlayArray[i] != "background") {
				$("#" + overlayArray[i] + ',[for="' + overlayArray[i] + '"]"').show();
				$("#" + overlayArray[i]).unbind("click");
				$("#" + overlayArray[i]).click(function(){
					$("#" + $(this).attr("id") + "Overlay").toggle();
				});
				$("#" + overlayArray[i]).attr('checked', true);
			}
        }
        else {
			$("#" + overlayArray[i] + ',[for="' + overlayArray[i] + '"]"').hide();
            $("#" + overlayArray[i] + "Overlay").html('');
            notFound++;
        }
    }
    if (notFound >= overlayArray.length) {
        $("#radarElements").hide().after('<div id="ajaxError"><h2>There was an error while retrieving the data.</h2><div class="errorfield">The specified area "' + id + '" or the given subfolder "' + subfolder + '" was not found.</div></div>');
    }
	if(id == "SE"){
		$(".specialRadarOverlay").show();
	}else{
		$(".specialRadarOverlay").hide();
	}
}

function createObjects(){
	console.info("createObjects");
    var i = 0;
    $(".picLink").each(function(index){
        objectArray.push({});
        objectArray[i].id = $(this).parent().parent().parent().parent().attr("class");
        objectArray[i].subfolder = extractSubfolder($(this));
        objectArray[i].name = objectArray[i].id + objectArray[i].subfolder;
        objectArray[i].picArray = picLoad(objectArray[i]);
        i++;
    });
}

function extractSubfolder(target){
    var classNames = target.attr("class").split(/\s+/);
    for (var i = 0; i < classNames.length; ++i) {
        if (classNames[i].indexOf("subfolder_") >= 0) {
            subfolder = classNames[i].substr(classNames[i].indexOf("subfolder_") + "subfolder_".length);
        }
    }
    console.info("subfolders extracted: "+subfolder);
    return subfolder;
}

function checkStartupStatus(id){
	if(startupVariable >= picLoadCounter-1){
		//set default overlays for south ecuador
    	setOverlays("SE");
		//set default radar images
	    setSouthEcuador();
	    $("#button_play").click();
	}else{
		startupVariable++;
	}
}

function picLoad(object){
	picLoadCounter++;
	console.info("loading URL: "+"readFolder.php?folder=areas/" + object.id + "/radarimages/" + object.subfolder);
    $.ajax({
        url: "readFolder.php?folder=areas/" + object.id + "/radarimages/" + object.subfolder,
        success: function(data){
        	console.info("picLoad successful: " + data);
            getObject(object.id, object.subfolder).picArray = data.split(", ");
			checkStartupStatus(object.id);
        },
        error: function(data){
            console.error("Could not load URL: " + url);
        }
    });
}

function initiate(obj, target){
	console.info("initiate: "+obj.name+"/"+target);
	obj.target = target;
	if(obj.picArray[0] != ""){
		activeContainer = target;
		activeObject = obj;
		obj.index = 0;
		$(target).addClass("zoom");
	    $(target).find("img").remove();
	    renderImages(obj, target, obj.index);
	    $("#picTitle").text(" Speed: " + speed / 1000);
	    $(target).find("img:nth-child(3)").show();
	    $("#dateOverlay").text($(target).find("img:nth-child(3)").attr("alt")).show();
	    $("#picIndex").text((obj.index + 1) + "/" + obj.picArray.length).show();
	}else{
		console.warn("picArray is empty at "+obj.name+"/"+target);
		$(target).css("background","url(images/no_rain.png) center center no-repeat");
		$(target).click(function(event) {
			event.preventDefault();
		});
	}
}

function checkIndex(array, i){
    var tmp;
    var test = "";
    if (i >= array.length) {
        tmp = i - array.length;
    }
    else {
        if (i < 0) {
            tmp = array.length + i;
        }
        else {
            tmp = i;
        }
    }
    return tmp;
}

function renderImages(obj, target, i){
    a = checkIndex(obj.picArray, i - 2);
    b = checkIndex(obj.picArray, i - 1);
    c = checkIndex(obj.picArray, i);
    d = checkIndex(obj.picArray, i + 1);
    e = checkIndex(obj.picArray, i + 2);
    $(target).append(addImgElement(obj, a)).append(addImgElement(obj, b)).append(addImgElement(obj, c)).append(addImgElement(obj, d)).append(addImgElement(obj, e));
}

function addImgElement(obj, i){
	if(obj.picArray[0] == ""){
		console.warn("picArray is empty!");
	}
    var tmpMeasures;
    if (obj.id == "SE") {
        tmpMeasures = ' width="' + $("#picContainer").width() + '" height="' + $("#picContainer").height() + '"';
    }
    else {
    	tmpMeasures = ' width="' + $(obj.target).width() + '" height="' + $(obj.target).height() + '"';
    }
    return '<img src="areas/' + obj.id + "/radarimages/" + obj.subfolder + "/" + obj.picArray[i] +
    //'?t=' +
    //serverTime +
    '" alt="' +
    parseFilename(obj.picArray[i]) +
    " " +
    txt +
    '"' +
    tmpMeasures +
    '/>';
}

function getObject(id, subfolder){
    for (var i = 0; i < objectArray.length; i++) {
        if (id == objectArray[i].id && subfolder == objectArray[i].subfolder) {
            return objectArray[i];
        }
    }
    return null;
}

function getFirstSubfolderFromObject(id){
    for (var i = 0; i < objectArray.length; i++) {
        if (id == objectArray[i].id) {
            return objectArray[i].subfolder;
        }
    }
    return null;
}

function parseFilename(file){
	var tmp = file.indexOf(".");
	year = file.substr(tmp-12, 4);
	month = file.substr(tmp-8, 2);
	day = file.substr(tmp -6, 2);
	hour = file.substr(tmp -4, 2);
	minute = file.substr(tmp -2, 2);
	timestring = day + "." + month + "." + year + ", " + hour + ":" + minute;
	return timestring;
}

function initiateTimer(){
	console.log("initiateTimer");
    t = window.setInterval('emulateClick()', speed);
}

function emulateClick(){
    $("#button_next").click();
	$("#picTitle").text($(activeContainer).find("img:nth-child(3)").attr("alt") + " / Speed: " + 1000 / speed);
	$("#dateOverlay").text($(activeContainer).find("img:nth-child(3)").attr("alt"));
}

function deleteTimer(){
    clearInterval(t);
    t = null;
}

function previous(target,id){
	$("#picIndex").text(checkIndex(activeObject.picArray,activeObject.index - 1) + "/" + activeObject.picArray.length);
	getObject(id, getFirstSubfolderFromObject(id)).index--;
	getObject(id, getFirstSubfolderFromObject(id)).index = checkIndex(getObject(id, getFirstSubfolderFromObject(id)).picArray, getObject(id, getFirstSubfolderFromObject(id)).index);
    $(target).prepend(addImgElement(getObject(id, getFirstSubfolderFromObject(id)), checkIndex(getObject(id, getFirstSubfolderFromObject(id)).picArray, getObject(id, getFirstSubfolderFromObject(id)).index - 2)));
    $(target).find("img:last-child").remove();
    $(target).find("img:nth-child(3)").show();
    $(target).find("img:nth-child(4)").hide();
    $(target).find("img:nth-child(5)").hide();
}

function next(target,id){
	$("#picIndex").text(checkIndex(activeObject.picArray,activeObject.index + 1) + "/" + activeObject.picArray.length);
	getObject(id, getFirstSubfolderFromObject(id)).index++;
	getObject(id, getFirstSubfolderFromObject(id)).index = checkIndex(getObject(id, getFirstSubfolderFromObject(id)).picArray, getObject(id, getFirstSubfolderFromObject(id)).index);
    $(target).append(addImgElement(getObject(id, getFirstSubfolderFromObject(id)), checkIndex(getObject(id, getFirstSubfolderFromObject(id)).picArray, getObject(id, getFirstSubfolderFromObject(id)).index + 2)));
    $(target).find("img:first-child").remove();
    $(target).find("img:nth-child(1)").hide();
    $(target).find("img:nth-child(2)").hide();
    $(target).find("img:nth-child(3)").show();
}

function setSouthEcuador(){
	console.info("South Ecuador set 1: CAXX");
	initiate(getObject("CAXX", getFirstSubfolderFromObject("CAXX")), "#specialRadarOverlay1");
	console.info("South Ecuador set 2: GUAXX");
    initiate(getObject("GUAXX", getFirstSubfolderFromObject("GUAXX")), "#specialRadarOverlay2");
	console.info("South Ecuador set 3: LOXX");
    initiate(getObject("LOXX", getFirstSubfolderFromObject("LOXX")), "#specialRadarOverlay3");
}

function checkFolder(){
    $("#sidebar > ul > li").each(function(index){
        loadFolder($(this).attr("class"));
    });
}

function loadFolder(id){
    $.ajax({
        url: "checkFolder.php?folder=areas/" + id,
        success: function(data){
            if (data == "0") {
                $("."+id).remove();
            }
        }
    });
}

function playAreasWithPictures(direction){
	for (var i = 0; i < objectArray.length; i++) {
		if (objectArray[i].picArray != undefined && objectArray[i].picArray != "" && getFirstSubfolderFromObject(objectArray[i].id) == objectArray[i].subfolder) {
			if (direction == "forward") {
				next(objectArray[i].target, objectArray[i].id);
			}else{
				previous(objectArray[i].target, objectArray[i].id);
			}
		}
	}
}

$(document).ready(function(){
    //check folder
    checkFolder();
    //create objects, depending on links at sidebar
    createObjects();
    //actions for play buttons
    $("#button_previous").click(function(){
        playAreasWithPictures("backward");
    });
    $("#button_next").click(function(){
		playAreasWithPictures("forward");
    });
    $("#button_play").click(function(){
        if (!t) {
            initiateTimer();
        }
        $(this).hide();
        $("#button_stop").show();
        $("#button_slower").show();
        $("#button_faster").show();
        $("#button_previous").hide();
        $("#button_next").hide();
    });
    $("#button_stop").click(function(){
        deleteTimer();
        $(this).hide();
        $("#button_play").show();
        $("#button_slower").hide();
        $("#button_faster").hide();
        $("#button_previous").show();
        $("#button_next").show();
    });
    $("#button_slower").click(function(){
        if (speed < 5000) {
            deleteTimer();
            speed = speed * 2;
            initiateTimer();
        }
    });
    $("#button_faster").click(function(){
        if (speed > 32) {
            deleteTimer();
            speed = speed / 2;
            initiateTimer();
        }
    });
});
