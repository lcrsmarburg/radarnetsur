//+----------------------------------------------+
//| name       : Simple Tooltip                  |
//| author     : runzheimer                      |
//| description: Displays "title" tag as tooltip |
//+----------------------------------------------+
jQuery.fn.simpleTooltip = function(){
    if ($(this).attr("title")) {
        var o = $(this[0]);
        var yPos;
        var xPos;
        var tmp = "";
        if(!$("#simpleTooltip_box").length){
        	$('*:not(* *)').append('<div id="simpleTooltip_box"><span class="simpleTooltip_arrow"><!-- arrow --></span><span class="simpleTooltip_arrowUpper"><!-- arrow --></span><div class="simpleTooltip_text"></div></div>');
        }
        o.mouseover(function(){
        	toolTipActive = true;
            if ($(this).attr("title").indexOf("::") >= 0) {
                tmp = "<p><strong>" + ($(this).attr("title").substr(0, $(this).attr("title").indexOf("::")) + "</strong></p>");
                tmp += $(this).attr("title").substr($(this).attr("title").indexOf("::") + 2);
            }
            else {
                tmp = $(this).attr("title");
            }
            $(this).data("title", tmp).attr("title", "");
            $(".simpleTooltip_text").html($(this).data("title"));
            if ($("#simpleTooltip_box").width() < $("#simpleTooltip_box").height()) {
                $("#simpleTooltip_box").css("min-width", $("#simpleTooltip_box").height());
            }
            var position = $(this).offset();
            $("#simpleTooltip_box").hide();
            xPos = position.left - 20;
            yPos = position.top - $("#simpleTooltip_box").height()*2;
            if (yPos < 0){
	        	yPos = position.top + $("#simpleTooltip_box").height()*2;
            }
            $("#simpleTooltip_box").css("top", yPos);
            $("#simpleTooltip_box").css("left", xPos);
            var elementHeight = $(this).height();
            setTimeout(function(){repositionElement(position,elementHeight)},10);
        });
        o.mouseout(function(){
        	toolTipActive = false;
            $(this).attr("title", $(this).data("title"));
            $("#simpleTooltip_box").hide();
        });
        $("#simpleTooltip_box").mouseout(function(){
            o.mouseout();
        });
    }
    function repositionElement(pos,elemHeight){
    	if(toolTipActive){
    		var newPosTop = pos.top - $("#simpleTooltip_box").height()-$("#simpleTooltip_box").css("padding-top").substr(0,$("#simpleTooltip_box").css("padding-top").length-2)-$("#simpleTooltip_box").css("padding-bottom").substr(0,$("#simpleTooltip_box").css("padding-bottom").length-2)-$(".simpleTooltip_arrow").height();
    		if (newPosTop < 0){
    			newPosTop = 0 + pos.top + elemHeight + $(".simpleTooltip_arrowUpper").height();
    			$(".simpleTooltip_arrow").hide();
    			$(".simpleTooltip_arrowUpper").show();
    		}else{
    			$(".simpleTooltip_arrow").show();
    			$(".simpleTooltip_arrowUpper").hide();
    		}
	    	$("#simpleTooltip_box").css("top", newPosTop);
	    	$("#simpleTooltip_box").show();
    	}
    }
};
