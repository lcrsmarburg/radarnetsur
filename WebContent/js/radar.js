var txt = "UTC";
var index = 0;
var indexCounter = 0;
var speed = 1000;
var id = "";
var picArray;
var path = "areas/";
var subfolder;
var t;
function setOverlays(id){
    $("#picMap img").attr("src", "areas/" + id + "/overlays/terrain.png");
    $("#roadsOverlay img").attr("src", "areas/" + id + "/overlays/vias.png");
    $("#riversOverlay img").attr("src", "areas/" + id + "/overlays/rios.png");
    $("#townsOverlay img").attr("src", "areas/" + id + "/overlays/pueblos.png");
    $("#riversOverlay").hide();
    $("#rivers").attr('checked', false);
    $("#roadsOverlay").show();
    $("#roads").attr('checked', true);
    $("#townsOverlay").show();
    $("#towns").attr('checked', true);
}

function fillPicArray(data){
    window.picArray = data;
}

function parseFilename(file){
    year = file.substr(4, 4);
    month = file.substr(8, 2);
    day = file.substr(10, 2);
    hour = file.substr(12, 2);
    minute = file.substr(14, 2);
    timestring = day + "." + month + "." + year + ", " + hour + ":" + minute;
    return timestring;
}

function initiateTimer(){
    t = window.setInterval('emulateClick()', speed);
}

function emulateClick(){
    $("#button_next").click();
}

function deleteTimer(){
    clearInterval(t);
}

function checkIndex(i){
    var tmp;
    if (i > picArray.length) {
        tmp = i - picArray.length;
    }
    else 
        if (i < 0) {
            tmp = picArray.length + i;
        }
        else {
            tmp = i;
        }
    return tmp;
}

function renderImages(i){
    a = checkIndex(i - 2);
    b = checkIndex(i - 1);
    c = checkIndex(i);
    d = checkIndex(i + 1);
    e = checkIndex(i + 2);
    $("#picContainer").append(addImgElement(a)).append(addImgElement(b)).append(addImgElement(c)).append(addImgElement(d)).append(addImgElement(e));
}

function addImgElement(i){
    return '<img src="' + path + id + "/radarimages/" + subfolder + "/" + picArray[i] +
    '?t=' +
    serverTime +
    '" alt="' +
    parseFilename(picArray[i]) +
    " " +
    txt +
    '"' +
    ' width="500" height="500"' +
    '/>';
}

function initiate(){
    index = 0;
    $("#picContainer img").remove();
    renderImages(index);
    $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt") + " / Speed: " + speed / 1000);
    $("#picContainer img:nth-child(3)").show();
    $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt")).show();
    $("#picIndex").text(index + "/" + picArray.length);
}

$(document).ready(function(){
    //actions for checkboxes
    $("#map").click(function(){
        $("#picMap").toggle();
    });
    $("#map").attr('checked', true);
    $("#roads").click(function(){
        $("#roadsOverlay").toggle();
    });
    $("#roads").attr('checked', false);
    $("#rivers").click(function(){
        $("#riversOverlay").toggle();
    });
    $("#rivers").attr('checked', false);
    $("#towns").click(function(){
        $("#townsOverlay").toggle();
    });
    $("#towns").attr('checked', true);
    $("#picContainer img:nth-child(3)").show();
    //actions for links
    $(".picLink").click(function(event){
        event.preventDefault();
		$("#radarElements h2").text($(this).parent().parent().parent().parent().find("p").text());
        if (t != undefined) {
            speed = 1000;
            $("#button_stop").click();
        }
        id = $(this).parent().parent().parent().parent().attr("class");
        var classNames = this.className.split(/\s+/);
        for (var i = 0; i < classNames.length; ++i) {
            if (classNames[i].indexOf("subfolder_") >= 0) {
                subfolder = classNames[i].substr(classNames[i].indexOf("subfolder_") + "subfolder_".length);
            }
        }
        setOverlays(id);
        $.get("readFolder.php?folder=areas/" + id + "/radarimages/" + subfolder, function(data){
            $("#ajaxError").remove();
            $("#radarElements").show();
            if (data == "") {
                $("#radarElements").hide().after('<div id="ajaxError"><h2>There was an error while retrieving the data.</h2><div class="errorfield">The specified area "' + id + '" or the given subfolder "' + subfolder + '" was not found.</div></div>');
            }
            else {
                fillPicArray(data.split(","));
                initiate();
            }
        });
    });
    $("#button_previous").click(function(){
        index--;
        index = checkIndex(index);
        $("#picContainer").prepend(addImgElement(checkIndex(index - 2)));
        $("#picContainer img:last-child").remove();
        $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt") + " / Speed: " + 1000 * speed);
        $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt"));
        $("#picIndex").text(index + "/" + picArray.length);
        $("#picContainer img:nth-child(3)").show();
        $("#picContainer img:nth-child(4)").hide();
        $("#picContainer img:nth-child(5)").hide();
    });
    $("#button_next").click(function(){
        index++;
        index = checkIndex(index);
        $("#picContainer").append(addImgElement(checkIndex(index + 2)));
        $("#picContainer img:first-child").remove();
        $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt") + " / Speed: " + 1000 / speed);
        $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt"));
        $("#picIndex").text(index + "/" + picArray.length);
        $("#picContainer img:nth-child(1)").hide();
        $("#picContainer img:nth-child(2)").hide();
        $("#picContainer img:nth-child(3)").show();
    });
    $("#button_play").click(function(){
        initiateTimer();
        $(this).hide();
        $("#button_stop").show();
        $("#button_slower").show();
        $("#button_faster").show();
        $("#button_previous").hide();
        $("#button_next").hide();
    });
    $("#button_stop").click(function(){
        deleteTimer();
        $(this).hide();
        $("#button_play").show();
        $("#button_slower").hide();
        $("#button_faster").hide();
        $("#button_previous").show();
        $("#button_next").show();
    });
    $("#button_slower").click(function(){
        if (speed < 5000) {
            deleteTimer();
            speed = speed * 2;
            initiateTimer();
        }
    });
    $("#button_faster").click(function(){
        if (speed > 32) {
            deleteTimer();
            speed = speed / 2;
            initiateTimer();
        }
    });
    $(".SE .picLink").click();
});
