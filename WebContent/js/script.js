var t;
var path = "radarimages/";
var subfolder = "CEXX/4hours/";
//var picExtension = "gif";
var txt = "UTC";
var index = 0;
var indexCounter = 0;
var speed = 1000;
var direction = "forward";


var overlay_LOXX_terrain = "overlays/terrain_LOXX.png";
var overlay_CUXX_terrain = "overlays/terrain_CUXX.png";
var overlay_CAXX_terrain = "overlays/terrain_CEXX.png";

var overlay_LOXX_roads = "overlays/vias_LOXX.png";
var overlay_CUXX_roads = "overlays/vias_CUXX.png";
var overlay_CAXX_roads = "overlays/vias_CEXX.png";

var overlay_LOXX_rivers = "overlays/rios_LOXX.png";
var overlay_CUXX_rivers = "overlays/rios_CUXX.png";
var overlay_CAXX_rivers = "overlays/rios_CEXX.png";

var overlay_LOXX_towns = "overlays/pueblos_LOXX.png";
var overlay_CUXX_towns = "overlays/pueblos_CUXX.png";
var overlay_CAXX_towns = "overlays/pueblos_CEXX.png";

var overlay_normal_roads = "overlays/vias_LOXX.png";
var overlay_normal_rivers = "overlays/rios_LOXX.png";
var overlay_normal_towns = "overlays/pueblos_LOXX.png";

var overlay_SE_terrain = "overlays/Overview_terrain.png";
var overlay_SE_roads = "overlays/Overview_box120.png";
var overlay_SE_rivers = "overlays/Overview_pueblos.png";
var overlay_SE_towns = "overlays/Overview_pueblos.png";

function parseFilename(file){
    year = "20" + file.substr(2, 2);
    month = file.substr(4, 2);
    day = file.substr(6, 2);
    hour = file.substr(8, 2);
    minute = file.substr(10, 2);
    timestring = day + "." + month + "." + year + ", " + hour + ":" + minute;
    return timestring;
}

function initiateTimer(){
    t = window.setInterval('emulateClick()', speed);
}

function emulateClick(){
    $("#button_next").click();
}

function deleteTimer(){
    clearInterval(t);
}

function checkIndex(i){
    var tmp;
    if (i > picArray.length) {
        tmp = i - picArray.length;
    }
    else 
        if (i < 0) {
            tmp = picArray.length + i;
        }
        else {
            tmp = i;
        }
    return tmp;
}

function renderImages(i){
    a = checkIndex(i - 2);
    b = checkIndex(i - 1);
    c = checkIndex(i);
    d = checkIndex(i + 1);
    e = checkIndex(i + 2);
    $("#picContainer").append(addImgElement(a)).append(addImgElement(b)).append(addImgElement(c)).append(addImgElement(d)).append(addImgElement(e));
}

function addImgElement(i){
    return '<img src="' + path + subfolder + picArray[i] + 
    //'.' + picExtension +
	'?t=' + serverTime + 
    '" alt="' +
    parseFilename(picArray[i]) +
    " " +
    txt +
    '"/>';
}

function initiate(){
	index = 0;
    $("#picContainer img").remove();
	renderImages(index);
    $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt"));
    $("#picContainer img:nth-child(3)").show();
    $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt")).show();
    $("#picIndex").text(index + "/" + picArray.length);
}

function setDefaultOverlays(){
	$("#picMap img").attr("src", overlay_normal_terrain);
	$("#roadsOverlay img").attr("src", overlay_normal_roads);
	$("#riversOverlay img").attr("src", overlay_normal_rivers);
	$("#townsOverlay img").attr("src", overlay_normal_towns);
	$("#riversOverlay").hide();
	$("#rivers").attr('checked', false);
	$("#roadsOverlay").show();
	$("#roads").attr('checked', true);
	$("#townsOverlay").show();
	$("#towns").attr('checked', true);
}

function setSpecialOverlay(){
	$("#picMap img").attr("src", overlay_SE_terrain);
	$("#roadsOverlay img").attr("src", overlay_SE_roads);
	$("#townsOverlay img").attr("src", overlay_SE_towns);
	//remove overlay options 
	$("#additionalControls").hide();
	//show roadoverlay ->
	$("#riversOverlay").hide();
	$("#picMap").show();
	$("#roadsOverlay").show();
	$("#townsOverlay").show();
}

$(document).ready(function(){
    $("#button_previous").click(function(){
        index--;
        index = checkIndex(index);
        if (direction == "backward") {
            $("#picContainer").prepend(addImgElement(checkIndex(index-2)));
            $("#picContainer img:last-child").remove();
            $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#picIndex").text(index + "/" + picArray.length);
        }
        else {
            $("#picContainer img").remove();
            renderImages(index);
            direction = "backward";
        }
        $("#picContainer img:nth-child(3)").show();
        $("#picContainer img:nth-child(4)").hide();
        $("#picContainer img:nth-child(5)").hide();
    });
    $("#button_next").click(function(){
        index++;
        index = checkIndex(index);
        if (direction == "forward") {
            $("#picContainer").append(addImgElement(checkIndex(index+2)));
            $("#picContainer img:first-child").remove();
            $("#picTitle").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#dateOverlay").text($("#picContainer img:nth-child(3)").attr("alt"));
            $("#picIndex").text(index + "/" + picArray.length);
        }
        else {
            $("#picContainer img").remove();
            renderImages(index);
            direction = "forward";
        }
        $("#picContainer img:nth-child(1)").hide();
        $("#picContainer img:nth-child(2)").hide();
        $("#picContainer img:nth-child(3)").show();
    });
    $("#button_play").click(function(){
        initiateTimer();
        $(this).hide();
        $("#button_stop").show();
        $("#button_slower").show();
        $("#button_faster").show();
        $("#button_previous").hide();
        $("#button_next").hide();
    });
    $("#button_stop").click(function(){
        deleteTimer();
        $(this).hide();
        $("#button_play").show();
        $("#button_slower").hide();
        $("#button_faster").hide();
        $("#button_previous").show();
        $("#button_next").show();
    });
    $("#button_slower").click(function(){
        deleteTimer();
        speed = speed * 2;
        initiateTimer();
    });
    $("#button_faster").click(function(){
        deleteTimer();
        speed = speed / 2;
        initiateTimer();
    });
    setSpecialOverlay();
    initiate();
    //+----------+
    //| overlays |
    //+----------+
    $("#map").click(function(){
        $("#picMap").toggle();
    });
    $("#map").attr('checked', true);
    $("#roads").click(function(){
        $("#roadsOverlay").toggle();
    });
    $("#roads").attr('checked', false);
    $("#rivers").click(function(){
        $("#riversOverlay").toggle();
    });
    $("#rivers").attr('checked', false);
    $("#towns").click(function(){
        $("#townsOverlay").toggle();
    });
    $("#towns").attr('checked', true);
    $("#picContainer img:nth-child(3)").show();
    $(".picLink").click(function(event){
    	event.preventDefault();
        if($(this).hasClass("CUXX")){
        	$("#content h2").text("Cuenca: "+$(this).text());
        	overlay_normal_terrain = overlay_CUXX_terrain;
        	overlay_normal_roads = overlay_CUXX_roads;
        	overlay_normal_rivers = overlay_CUXX_rivers;
        	overlay_normal_towns = overlay_CUXX_towns;
        	setDefaultOverlays();
        	$("#additionalControls").show();
        	if($(this).hasClass("h2")){
        		picArray = picCUXXh2Array;
        		subfolder = "CUXX/2hours/";
            }else if($(this).hasClass("h3")){
            	picArray = picCUXXh3Array;
            	subfolder = "CUXX/3hours/";
            }
        }else if($(this).hasClass("LOXX")){
        	$("#content h2").text("Loja: "+$(this).text());
        	overlay_normal_terrain = overlay_LOXX_terrain;
        	overlay_normal_roads = overlay_LOXX_roads;
        	overlay_normal_rivers = overlay_LOXX_rivers;
        	overlay_normal_towns = overlay_LOXX_towns;
        	setDefaultOverlays();
        	$("#additionalControls").show();
        	if($(this).hasClass("h2")){
        		picArray = picLOXXh2Array;
        		subfolder = "LOXX/2hours/";
            }else if($(this).hasClass("h3")){
            	picArray = picLOXXh3Array;
            	subfolder = "LOXX/3hours/";
            }
        }else if($(this).hasClass("CAXX")){
        	$("#content h2").text("Celica: "+$(this).text());
        	overlay_normal_terrain = overlay_CAXX_terrain;
        	overlay_normal_roads = overlay_CAXX_roads;
        	overlay_normal_rivers = overlay_CAXX_rivers;
        	overlay_normal_towns = overlay_CAXX_towns;
        	setDefaultOverlays();
        	$("#additionalControls").show();
        	if($(this).hasClass("h2")){
        		picArray = picCAXXh2Array;
        		subfolder = "CAXX/2hours/";
            }else if($(this).hasClass("h3")){
            	picArray = picCAXXh3Array;
            	subfolder = "CAXX/3hours/";
            }
	    }else if($(this).hasClass("SE")){
	    	$("#content h2").text("Southern ecuador: "+$(this).text());
	    	overlay_normal_terrain = overlay_SE_terrain;
	    	overlay_normal_roads = overlay_SE_roads;
        	overlay_normal_rivers = overlay_SE_rivers;
        	overlay_normal_towns = overlay_SE_towns;
        	setSpecialOverlay();
	    	if($(this).hasClass("h4")){
	    		picArray = picSEArray;
	    		subfolder = "CEXX/4hours/";
	    	}
	    }
		alert(subfolder);
        initiate();
    });
});
