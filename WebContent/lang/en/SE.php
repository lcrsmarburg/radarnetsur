<p>The DFG (German Research Foundation) transfer project "Operational
	rainfall monitoring in southern Ecuador" (BE 1780/31-1), called
	"RadarNet-Sur", contains 3 X-Band weather radars (9.4GHz; GUAXX, LOXX
	and CAXX). The project is a cooperation between the
	Phillipps-University of Marburg/ Germany and the Provincial Government
	of Loja/ Ecuador. Main partners operating the radars are the Technical
	University of Loja and the Empresa de Telecomunicaciones, Agua Potable
	y Alcantarillado de Cuenca.</p>
<p>The 3 weather radars form a red measuring the precipitation
	intensities in southern Ecuador. The Webpage provides the real-time
	radar images of all radars for the last 3 hours.</p>
<p>
	The <b>radar GUAXX</b> is operated by the Provincial Government of Loja
	(GPL), installed at the Guachaurco mountain peak (3100m asl), and
	covers the western parts of southern Ecuador, including big parts of
	the coastal province EL ORO and the Andean province LOJA. <br />The
	radar from SELEX-Gematronic (Germany) has a maximum range of 100km
	(radio) and provides user- defined images with different ranges and
	radial resolutions (100m to 500m). <br /> The <b>different ranges and
		resolutions</b> can be visualized <b>clicking the specific radar name</b>
	(e.g. Celica: "Loop latest 3 hours") on the left border of the Webpage
	or directly by <b>clicking on the radar range cycle in the map</b>.
</p>
<p>
	The <b>radar LOXX</b> is operated by the Technical University of Loja
	(UTPL), installed at the El Tiro mountain peak (2850m asl), and covers
	the eastern and northern parts of the Andean province of LOJA as well
	as the northern parts of the Amazonian province of ZAMORA-CHINCHIPE.<br />
	The radar from DHI (Dansk Hydrolosk Institut) has a maximum range of
	60km (radio) and provides automatically generated images with different
	ranges and radial resolutions (100m to 500m). <br /> The <b>different
		ranges and resolutions</b> can be visualized <b>clicking the specific
		radar name</b> (e.g. Loja: "Loop latest 3 hours") on the left border
	of the Webpage or directly by <b>clicking on the radar range cycle in
		the map</b>.
</p>
<p>
	The <b>radar CAXX</b> is operated by the Empresa de Telecomunicaciones,
	Agua Potable y Alcantarillado de Cuenca (ETAPA) and will be installed
	at the Paraguillas mountain peak (4440m asl). It covers the Andean
	provinces of AZUAY and CA�AR as well as the southern parts of the
	coastal province GUAYAS.<br />The radar from SELEX-Gematronic (Germany)
	has a maximum range of 100km (radio) and provides user- defined images
	with different ranges and radial resolutions (100m to 500km). <br />
	The <b>different ranges and resolutions</b> can be visualized <b>clicking
		the specific radar name</b> (e.g. Cajas: "Loop latest 3 hours") on the left border of
	the Webpage or directly by <b>clicking on the radar range cycle in the
		map</b>.
</p>
<p>For more detailed information of all radar types, please push the
	"Radar specification" button below "Instruments" on the left border of
	the Webpage.</p>
