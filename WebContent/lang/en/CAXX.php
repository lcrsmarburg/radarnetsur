<p>
	Real-time observation of rain intensity for the last 3 hours, based on
	SELEX-Gematronic X-band radar with a range of 100km and a resolution of
	500m. <br /> The images are uncalibrated and show the relative
	intensity and the distribution of rainfall. <br /> Note: The
	sensitivity capturing rainfall may vary with time, atmospheric
	conditions and distance from the radar.
</p>
<p>
	The radar <b>image animation</b> can be <b>accelerated or decelerated
		by using the buttons below the map</b>.
</p>
<p>
	To zoom in <b>click on the radar range cycle in the map</b>. <br /> To
	return to <b>main screen</b> push the <b>"Overview" button</b> below
	"Southern Ecuador" on the left border of the Webpage.
</p>
