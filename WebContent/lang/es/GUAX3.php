<p>
	Observaciones de la intensidad de la lluvia durante los &uacute;ltimos
	3 horas en tiempo real, basado en el SELEX-Gematronic radar de banda X
	con un <b>alcance de 20km</b> y un <b>resoluci&oacute;n de 100m</b>. <br />
	Los im&aacute;genes no son calibradas y muestran la intensidad relativa
	y la distribuci&oacute;n de la precipitaci&oacute;n. <br /> <b>Nota:</b>
	La sensibilidad del radar para medir la precipitaci&oacute;n puede
	disminuir con respecto al tiempo de duraci&oacute;n del instrumento de
	emisi&oacute;n (magnetron) y con la distancia.
<p>
	<b>La animaci&oacute;n de las im&aacute;genes</b> del radar se puede <b>acelerar
		o decelerar usando los botones por debajo del mapa</b>.
</p>
<p>
	Para <b>regresar a la p&aacute;gina principal</b>, presiona la
	opci&oacute;n <b>"Overview"</b> por debajo de "Southern Ecuador" en la
	parte izquierda de la pagina web.
</p>
