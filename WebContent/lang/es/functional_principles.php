<h4>Radar meteorologico - como funciona?</h4>
<p>
	El Radar emite pulsos fuertes de ondas electro-magneticas. Estas ondas estan reflejidas por las gotas de lluvia. La reflexion esta captada por la misma antena y la potencia de la reflexion es proporcional a la intensidad de la precipitacion. Seg&uacute;n la posicion de la antena y la demora de la se&ntilde;al, la ubicacion de la precipitacion puede ser determinada. Esta informacion esta processada en una computadora para producir mapas visibles de la distribucion de la lluvia.
	<br/>
	Asi, el radar puede facilitar informacion que requeria miles de pluviometros registrados cada cinco minutos.
</p>
<p>
	Problemas:
	<br/>
	<b>Clutter fields (areas de ruido)</b>
	<br/>
	S&iacute; los rayos del radar enfrentan obstrucciones como las monta&ntilde;as, reflexiones fuertes ocurren. Estas reflexiones estan estaticas, no varian en el tiempo. Por ello, el procesador puede filtrar estas se&ntilde;ales y las &aacute;reas estan interpoladas, para dar una estimacion de la precipitacion ahi.
</p>
<p>
	<b>Beam shadow (obstruccion de los rayos)</b>
	<br/>
	En el caso que los rayos estan bloqueados por las monta&ntilde;as, pueden occurir valles detras, que no estan observadas por el radar. Este efecto normalmente solo causa problemas con nubes muy bajas.
</p>
<p>
	<b>Weakening of the radar beam - atenuacion de los rayos</b>
	<br/>
	Cuando los rayos han cruzados un campo de precipitacion, la energia disminuye. Entonces un segundo campo de lluvia detras del primero puede ser indicada menos fuerte como es en realidad.
</p>