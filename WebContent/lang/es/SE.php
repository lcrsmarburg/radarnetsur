<p>
	El proyecto transfer de la DFG (Fundaci&oacute;n Alemana para la investigaci&oacute;n
	científica) "Operational rainfall monitoring in southern Ecuador" (BE
	1780/31-1), denominado como "RadarNet-Sur", contiene 3 radares
	meteorol&oacute;gicos de banda X (9.4GHz; GUAXX, LOXX and CAXX). El proyecto
	es una cooperaci&oacute;n entre la Philipps-Universidad Marburg/ Alemania y el
	Gobierno Provincial de Loja /Ecuador. Los socios principales del
	proyecto operando los radares, son la Universidad T&eacute;cnica Particular de
	Loja y la Empresa de Telecomunicaciones, Agua Potable y Alcantarillado
	de Cuenca. <br /> Los 3 radares forman una red, la cual mide la
	intensidad de la precipitaci&oacute;n en el Sur del Ecuador. La pagina web
	prepara im&aacute;genes de los radares de las 3 &uacute;ltimas horas en tiempo real.
</p>
<p>
	El radar GUAXX opera el Gobierno Provincial de Loja (GPL), el cual esta
	instalado en el Cerro Guachaurco (3100msnm), y cubre las partes
	occidentales del Sur del Ecuador, incluyendo grandes partes de la
	provincia costera EL ORO y de la provincia andina LOJA. <br /> El radar
	de SELEX-Gematronic (Alemania) tiene un alcance m&aacute;ximo de 100km (radio)
	y prepara 3 im&aacute;genes definidas con resoluciones radiales diferentes
	(100m hasta 500m). <br /> Los <b>alcances y resoluciones diferentes</b> se
	pueden visualizar <b>presionando el nombre del radar especifico</b> (p. ej.
	Celica: "Loop latest 3 hours") en la parte izquierda de la pagina web o
	directo <b>presionando el circulo de alcance en el mapa</b>.
</p>
<p>
	El radar LOXX opera la Universidad T&eacute;cnica Particular de Loja (UTPL),
	el cual est&aacute; instalado en el Cerro El Tiro (2850msnm), y cubre las
	partes orientales y nortes de la provincia andina LOJA y la parte norte
	de la provincia amaz&oacute;nica ZAMORA-CHINCHIPE. <br /> El radar de DHI
	(Dansk Hydrolosk Institut, Dinamarca) tiene un alcance m&aacute;ximo de 60km
	(radio) y prepara 3 im&aacute;genes pre-ajustadas con resoluciones radiales
	diferentes (100m hasta 500m). <br /> Los <b>alcances y resoluciones diferentes</b> se
	pueden visualizar <b>presionando el nombre del radar especifico</b> (p. ej.
	Celica: "Loop latest 3 hours") en la parte izquierda de la pagina web o
	directo <b>presionando el circulo de alcance en el mapa</b>.
</p>
<p>
	El radar CAXX opera la Empresa de Telecomunicaciones, Agua Potable y
	Alcantarillado de Cuenca (ETAPA), el cual est&aacute; instalado en el Cerro
	Paraguillas (4440msnm). Este radar cubre las provincias andinas AZUAY y
	CA&Ntilde;AR y tambi&eacute;n partes del sur de la provincia costera GUAYAS. <br />
	El radar de SELEX-Gematronic (Alemania) tiene un alcance m&aacute;ximo de
	100km (radio) y prepara 3 im&aacute;genes definidas con resoluciones radiales
	diferentes (100m hasta 500m). <br /> Los <b>alcances y resoluciones diferentes</b> se
	pueden visualizar <b>presionando el nombre del radar especifico</b> (p. ej.
	Celica: "Loop latest 3 hours") en la parte izquierda de la pagina web o
	directo <b>presionando el circulo de alcance en el mapa</b>.

</p>
<p>Para obtener mas informaci&oacute;n t&eacute;cnica sobre los radares, presiona la
	opci&oacute;n "Radar specification" por debajo de "Instruments" en la parte
	izquierda de la pagina web.</p>
